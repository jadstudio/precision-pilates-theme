<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Review', 'pp'),
        'description' => __('Add review block', 'pp'),
        'tab' => __('Sections', 'pp'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-format-status',
    )
);
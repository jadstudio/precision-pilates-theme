<?php if (!defined('FW')) die( 'Forbidden' ); ?>

<div class="review-block hotfix-row">
    <div class="container">
        <div class="reviewer-info">
            <div class="name">John P.</div>
            <div class="address">Newmarket, ON</div>
            <div class="date">February 8, 2017</div>
            <div class="rate">
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star"></span>
                <span class="fa fa-star-half-o"></span>
                <span class="fa fa-star-o"></span>
            </div>
        </div>

        <div class="review">
            <h2 class="title">My wife and I couldn’t be happier.</h2>
            <span class="sub-title">Window & Gutter Cleaning review in Toronto</span>
            <div class="review-content">
                <p>“Michael and Saeed provided me with a quick estimate and booking. The timing of it all couldn't of
                    been any better. They both did a great job. Professional, strong work ethic and great attention to 
                    detail. My wife and I couldn't be happier. The windows were so clean and shinny that they looked 
                    like I didn't have any windows! They even smelled nice! I would highly recommend these guys. 
                    Excellent work! Very pleased!”
                </p>
            </div>
        </div>
    </div>
</div>

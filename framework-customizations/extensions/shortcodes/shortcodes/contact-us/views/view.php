<?php $address = get_option('pt_address');
$phone = get_option('pt_phone_number');
$email = get_option('pt_email'); ?>
<div class="contact-us hotfix-row">
    <div class="container">
        <ul class="contacts">
            <li class="phone">
                <a href="tel:9(201) 417-3068">
                    <i class="fa fa-mobile"></i>

                    <div class="wrapper">
                        <span class="line-decorated"><?php echo $phone;?></span>
                    </div>
                </a>
            </li>

            <li class="email">
                <a href="mailto:<?php echo $email;?>">
                    <i class="fa fa-envelope-o"></i>

                    <div class="wrapper">
                        <span class="line-decorated"><?php echo $email;?></span>
                    </div>
                </a>
            </li>

            <li class="address">
                <a href="">
                    <i class="fa fa-map-marker"></i>

                    <div class="wrapper">
                        <span class="line-decorated">115 Rutherford Road,</span>
                        <span class="line-normal">Mahwah, NJ  07430</span>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>
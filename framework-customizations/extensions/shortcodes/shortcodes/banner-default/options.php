<?php if (!defined('FW')) die('Forbidden');
$options = array(
    'top_image' => array(
        'desc'  => __( 'Either upload a new, or choose an existing image from your media library', 'fw' ),
        'type'  => 'upload'
    ),

    'title' => array(
        'label' => __('Title text', '{domain}'),
        'type' => 'text'
    ),

    'after_title' => array(
        'label' => __('After title text', '{domain}'),
        'type' => 'text'
    ),
    
    'text' => array(
        'type'   => 'wp-editor',
        'label'  => __( 'Content', 'fw' ),
        'desc'   => __( 'Enter some content for this texblock', 'fw' )
    )
);
<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

if ( empty( $atts['top_image'] ) ) {
    $image = fw_get_framework_directory_uri('/static/img/no-image.png');
} else {
    $image = $atts['top_image']['url'];
}
?>

<div class="banner-default hotfix-row">
    <div class="container">
        <div class="intro">
            <?php if(!empty( $atts['top_image'])) { ?> 
                <div class="img-wrapper">
                    <img src="<?php echo esc_attr($image); ?>" alt="icon">
                </div>
            <?php } ; ?>
            <h2 class="h1 title_default"><?php echo $atts['title']; ?></h2>
            <span class="page-name"><?php echo $atts['after_title']; ?></span>
            <?php echo do_shortcode( $atts['text'] ); ?>
        </div>
    </div>
</div>
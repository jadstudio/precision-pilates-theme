<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Banner Default', 'pp'),
        'description' => __('Banner Default', 'pp'),
        'tab' => __('Jumbotrones', 'pp'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-format-image',
    )
);
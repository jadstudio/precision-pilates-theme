<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$image = $atts['image']['url'];

?>

<div class="about-block hotfix-row">
    <div class="container">
        <div class="img-container">
            <div class="img-wrap">
                <img src="<?php echo esc_attr($image); ?>" alt="">
            </div>
        </div>

        <div class="info-block">
            <h2><?php echo $atts['title']; ?></h2>
            <p><?php echo $atts['content_text']; ?></p>
        </div>
    </div>
</div>

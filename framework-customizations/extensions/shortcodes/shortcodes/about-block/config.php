<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('About', 'pp'),
        'description' => __('All block', 'pp'),
        'tab' => __('Sections', 'pp'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-groups',
    )
);
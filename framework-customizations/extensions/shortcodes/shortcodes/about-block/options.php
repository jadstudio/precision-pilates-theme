<?php if (!defined('FW')) die('Forbidden');
$options = array(
    'image' => array(
        'desc'  => __( 'Either upload a new, or choose an existing image from your media library', 'fw' ),
        'type'  => 'upload'
    ),

    'title' => array(
        'label' => __('Title text', '{domain}'),
        'type' => 'text'
    ),

    'content_text' => array(
        'label' => __('Content text', '{domain}'),
        'type' => 'textarea'
    )
);
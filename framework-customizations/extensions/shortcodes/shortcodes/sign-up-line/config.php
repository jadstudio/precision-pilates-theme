<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('"View Shedule" Line', 'pp'),
        'description' => __('Add "View Shedule" Decorated Line', 'pp'),
        'tab' => __('Sections', 'pp'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-welcome-write-blog',
    )
);
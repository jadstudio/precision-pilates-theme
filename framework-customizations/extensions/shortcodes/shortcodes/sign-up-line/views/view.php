<?php if (!defined('FW')) die( 'Forbidden' ); ?>

<div class="sign-up-line hotfix-row">
    <div class="container">
        <div class="info-block">
            <i class="fa fa-pencil-square-o"></i>
            
            <div class="wrapper">
                <h3 class="title">Book now</h3>
                <span class="sub-title">Get Started today and Book Your next appointment</span>
            </div>
        </div>

        <div class="sign-up-block">
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
                <form action="//precisionpilatesnj.us16.list-manage.com/subscribe/post?u=8ce66e688912ab0b81ec9437f&amp;id=7d5761e385" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll" class="input-wrap">
                        <div class="mc-field-group">
                            <i class="fa fa-envelope"></i>
                            <input type="email" placeholder="Enter your email" name="EMAIL" class="required email" id="mce-EMAIL">
                        </div>

                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8ce66e688912ab0b81ec9437f_7d5761e385" tabindex="-1" value=""></div>
                        <div class="clear"><input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="button"></div>

                        <div id="mce-responses" class="clear mce-responses">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    </div>
                </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
            <script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            <!--End mc_embed_signup-->
        </div>
    </div>
</div>
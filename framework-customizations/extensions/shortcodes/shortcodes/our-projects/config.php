<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Our Projects', 'jad'),
        'description' => __('Our projects section', 'jad'),
        'tab' => __('Sections', 'jad'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-admin-page',
    )
);
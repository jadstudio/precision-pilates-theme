<section class="our-projects hotfix-row">
    <div class="container">
        <h2 class="title title_default"><span>Наши проекты</span></h2>

        <ul class="projects-list four-column-wrapper">
            <li>
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ;?>/images/pic3.jpg" alt="some project name">
                    <div class="info">
                        <span class="btn btn_default">Смотреть</span>
                    </div>
                </a>
                <h3>Web Design</h3>
            </li>
            <li>
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ;?>/images/pic4.jpg" alt="some project name">
                    <div class="info">
                        <span class="btn btn_default">Смотреть</span>
                    </div>
                </a>
                <h3>Landing Pages</h3>
            </li>
            <li>
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ;?>/images/pic5.jpg" alt="some project name">
                    <div class="info">
                        <span class="btn btn_default">Смотреть</span>
                    </div>
                </a>
                <h3>Wordpress</h3>
            </li>
            <li>
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ;?>/images/pic6.jpg" alt="some project name">
                    <div class="info">
                        <span class="btn btn_default">Смотреть</span>
                    </div>
                </a>
                <h3>E-Commerce</h3>
            </li>
        </ul>

        <div class="last-project two-column-wrapper">
            <div class="wrap">
                <h3>Название последнего проекта</h3>
                <p>Сдесь краткое описание последнего проекта. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci alias,
                    animi cumque deleniti dolores earum enim eos ex facere hic illum iste, itaque iusto maiores maxime
                    minus molestias mollitia natus nisi obcaecati, porro provident quisquam reiciendis repellat
                    repellendus reprehenderit similique temporibus voluptatibus? Nostrum, quia quod! Deleniti,
                    eveniet odio?
                </p>

                <ul class="technologies">
                    <li><i class="fa fa-html5"></i></li>
                    <li><i class="fa fa-css3"></i></li>
                    <li><i class="fa fa-wordpress"></i></li>
                </ul>
            </div>

            <div class="wrap">
                <div class="computer">
                    <a class="screen" title="Смотреть работу" href="single-project.html">
                        <img src="<?php echo get_template_directory_uri() ;?>/images/art.mobilesmsmarketing.ca.png" alt="artmobilemarketing">
                    </a>
                </div>
            </div>
        </div>

        <div class="info">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci, architecto, autem debitis
                ipsa, minima neque nulla possimus quis repellat rerum ut! Aut dignissimos enim eum libero officiis
                optio quia!
            </p>
            <a href="#" class="btn btn_default">Все проекты</a>
        </div>
    </div>
</section>
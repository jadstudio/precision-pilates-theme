<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'tabs' => array(
		'type'          => 'addable-popup',
		'label'         => __( 'Tabs', 'pp' ),
		'popup-title'   => __( 'Add/Edit FAQ', 'pp' ),
		'desc'          => __( 'Create your FAQ', 'pp' ),
		'template'      => '{{=tab_title}}',
		'popup-options' => array(
			'tab_title' => array(
				'type'  => 'text',
				'label' => __('Question', 'fw')
			),
			'tab_content' => array(
				'type'  => 'textarea',
				'label' => __('Answer', 'fw')
			)
		),
	)
);
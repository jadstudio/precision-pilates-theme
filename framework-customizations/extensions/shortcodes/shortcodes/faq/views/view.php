<?php if (!defined('FW')) die( 'Forbidden' ); ?>

<div class="faq-block">
	<h3>Frequently Asked Questions</h3>
	<ul>
		<?php foreach ($atts['tabs'] as $key => $tab) : ?>
			<li>
				<span class="question"><?php echo $tab['tab_title']; ?></span>
				<p class="answer"><?php echo do_shortcode( $tab['tab_content'] ) ?></p>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
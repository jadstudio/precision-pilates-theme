<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('"Why Pilates" Block', 'pp'),
        'description' => __('Add "why pilates" block', 'pp'),
        'tab' => __('Sections', 'pp'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-format-status',
    )
);
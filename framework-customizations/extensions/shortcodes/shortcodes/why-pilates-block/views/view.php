<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}?>

<div class="why-pilates-block hotfix-row">
    <div class="container">
        <h2 class="title"><?php echo $atts['jad_spb_main_title'];?></h2>
        <ul class="icon-list">
<?php foreach ($atts['jad_spb'] as $kay => $block) {
    if ( !empty( $block['jad_spb_img'] ) ){
        $img_url = '<img style="background-color:'. $block['jad_spb_bg_color'] .'" src="'. $block['jad_spb_img']['url'].'" alt="pilates-ico"/>';
    }else{
        $img_url ='';
    }
    ?>
            <li class="list-item">
                <div class="icon-wrap" >
                    <?php echo $img_url; ?>
                </div>

                <div class="icon-text-wrap">
                    <span><?php echo $block['jad_spb_title'];?></span>
                </div>
            </li>
    <?php }?>
        </ul>
    </div>
</div>
<?php if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'jad_spb_main_title' => array(
        'type' => 'text',
        'label' => __('Title', 'fw')
    ),
    'jad_spb' => array(
        'type' => 'addable-popup',
        'label' => __('Image with text', 'fw'),
        'popup-title' => __('Add/Edit Section', 'fw'),
        'desc' => __('Create your block', 'fw'),
        'template' => '{{=jad_spb_title}}',
        'popup-options' => array(
            'jad_spb_img' => array(
                'type' => 'upload',
                'value' => array(),
                'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                'label' => __('Img', ''),
                'desc' => __('Img for block', ''),
                'images_only' => true,

            ),
            'jad_spb_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'jad_spb_bg_color' => array(
                'type' => 'color-picker',
                'value' => '',
                'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                // palette colors array

                'label' => __('Img background color', ''),
                'desc' => __('Color for background img', ''),
            ),
            'jad_spb_content' => array(
                'type' => 'textarea',
                'label' => __('Content', 'fw')
            ),
        ),

    )
);
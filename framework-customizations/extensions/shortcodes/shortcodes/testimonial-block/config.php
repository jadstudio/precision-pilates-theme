<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title' => __('Testimonial', 'pp'),
        'description' => __('Add testimonial block', 'pp'),
        'tab' => __('Sections', 'pp'),
        'popup_size' => 'medium',
        'icon' => 'dashicons dashicons-format-status',
    )
);
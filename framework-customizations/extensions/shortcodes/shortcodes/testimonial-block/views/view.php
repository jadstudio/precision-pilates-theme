<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

if ( empty( $atts['image'] ) ) {
    $image = fw_get_framework_directory_uri('/static/img/no-image.png');
} else {
    $image = $atts['image']['url'];
}
?>

<div class="testimonial-block hotfix-row">
    <div class="container">
        <?php if($atts['image']) { ?>
        <div class="photo">
            <div class="img-wrap">
                <img src="<?php echo esc_attr($image); ?>" alt="<?php echo esc_attr($atts['name']); ?>"/>
            </div>
        </div>
        <?php } ; ?>

        <div class="testimonial">
            <div class="testimonial-content">
                <p><?php echo $atts['testimonial']; ?></p>
            </div>
            <span class="author"> - <?php echo $atts['name']; ?></span>
        </div>
    </div>
</div>

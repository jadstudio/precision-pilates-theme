<?php if ( ! defined( 'FW' ) ) {
    die( 'Forbidden' );
}

$options = array(
    'image' => array(
        'label' => __( 'Photo', 'dd' ),
        'desc'  => __( 'Either upload a new, or choose an existing image from your media library', 'fw' ),
        'type'  => 'upload'
    ),
    'name'  => array(
        'label' => __( 'Name', 'dd' ),
        'desc'  => __( 'Name of the person', 'dd' ),
        'type'  => 'text',
        'value' => ''
    ),
    'testimonial'  => array(
        'label' => __( 'Testimonial', 'fw' ),
        'desc'  => __( 'Enter testimonial text', 'fw' ),
        'type'  => 'textarea',
        'value' => ''
    )
);
<?php if (!defined('FW')) {
    die('Forbidden');
}

$options = array(
    'jad_sliders' => array(
        'type' => 'addable-popup',
        'label' => __('Slide', 'fw'),
        'popup-title' => __('Add/Edit Slide', 'fw'),
        'desc' => __('Create your slide', 'fw'),
        'template' => '{{=jad_slide_title}}',
        'popup-options' => array(
            'jad_slide_img' => array(
                'type'  => 'upload',
                'value' => array(),
                'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
                'label' => __('Img', ''),
                'desc'  => __('Img for slider', ''),
                'images_only' => true,

            ),
            'jad_slide_title' => array(
                'type' => 'text',
                'label' => __('Title', 'fw')
            ),
            'jad_slider_color_title' => array(
                'type' => 'color-picker',
                'value' => '',
                'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                // palette colors array

                'label' => __('Title color', ''),
                'desc' => __('Color for title', ''),
                'help' => __('Help tip', ''),
            ),
            'jad_slider_content' => array(
                'type' => 'textarea',
                'label' => __('Content', 'fw')
            ),

            'jad_slider_color_content' => array(
                'type' => 'color-picker',
                'value' => '',
                'attr' => array('class' => 'custom-class', 'data-foo' => 'bar'),
                // palette colors array

                'label' => __('Content color', ''),
                'desc' => __('Color for Content', ''),
                'help' => __('Help tip', ''),
            ),
        ),
    )
);
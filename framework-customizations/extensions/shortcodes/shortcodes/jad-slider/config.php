<?php
if (!defined('FW')) die('Forbidden');

$cfg = array(
    'page_builder' => array(
        'title'         => __('JAD Slider', 't'),
        'description'   => __('Simple slider', 't'),
        'tab'           => __('Jumbotrones', 't'),
        'popup_size'    => 'medium', // can be large, medium or small
    )
);
<div class="jad-slider-block hotfix-row">
    <ul class="slider bxslider">
        <?php foreach ($atts['jad_sliders'] as $kay => $slide) {

            if (isset($slide['jad_slide_img']['url'])) {
                ?>
                <li class="slide"
                    style="background-image: url('<?php echo esc_attr($slide['jad_slide_img']['url']); ?>')">
                    <div class="shadow-layer" style=" background-color: rgba(73, 73, 73, 0);">
                        <h2 class="slider-title"
                            style="color: <?php echo $slide['jad_slider_color_title']; ?>"><?php echo $slide['jad_slide_title']; ?></h2>
                    </div>
                </li>
            <?php }
        } ?>
    </ul>
</div>
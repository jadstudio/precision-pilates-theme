<?php
/**
 * The template for displaying 404 pages (Not Found)
 */
get_header(); ?>
    <div>
        <div class="container">
            <header>
                <h1 class="title title_default"><?php _e( 'Oops! That page can&rsquo;t be found.', '' ); ?></h1>
            </header>

            <div>
                <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', '' ); ?></p>

                <?php get_search_form(); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
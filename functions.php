<?php
function jad_theme_scripts()
{
    wp_enqueue_style('pp-theme-style', get_stylesheet_uri());
    wp_enqueue_script('pp-all-js', get_template_directory_uri() . '/js/all.js', array( 'jquery' ));
}
add_action('wp_enqueue_scripts', 'jad_theme_scripts');

register_nav_menus( array(
    'header_menu' => 'Header menu',
    'footer_menu' => 'Footer menu'
) );


add_action( 'after_setup_theme', 'pt_after_setup' );

function pt_after_setup(){
    add_theme_support('menus');
    add_theme_support('custom-logo');
    add_theme_support('post-thumbnails');
    add_theme_support('html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ));
    
}

add_action( 'widgets_init', 'pt_widgets_init' );

function pt_widgets_init(){

    register_sidebar( array(
        'name'          => 'Footer about',
        'id'            => 'footer_1',
        'before_widget' => '<div class="widget-area about-us-block">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-area-title">',
        'after_title'   => '</h4>',
    ) );

}

require get_template_directory() . '/inc/customizer.php';

function phone_filter($phone){

    if ($phone[0] == '+'){
        $phone_clear = '+'.preg_replace('~\D+~','',$phone);
        return $phone_clear;
    }else{
        $phone_clear = '+1'.preg_replace('~\D+~','',$phone);
        return $phone_clear;
    }
}
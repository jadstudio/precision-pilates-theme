(function ($) {
    
    wp.customize('pt_email', function (value) {
        value.bind(function (to) {
            if ('to' == '') {
                $('.email').hide();
            } else {
                $('.email').show();
                $('.email').find('a').attr('href', 'mailto:' + to);
                $('.email').find('a').text(to);
            }
        });
    });

    wp.customize('pt_phone_number', function (value) {
        value.bind(function (to) {
            if ('to' == '') {
                $('.phone-number').hide();
            } else {
                $('.phone-number').show();
                $('.phone-number').find('a').attr('href', 'tel:' + to);
                $('.phone-number').find('a').text(to);
            }
        });
    });

    wp.customize('pt_address', function (value) {
        value.bind(function (to) {
            if ('to' == '') {
                $('.address').hide();
            } else {
                $('.address').show();
                $('.address').find('span').html(to);
            }
        });
    });

    wp.customize('pt_copyright', function (value) {
        value.bind(function (to) {
            if ('to' == '') {
                $('.copy').hide();
            } else {
                $('.copy').show();
                $('.copy').find('p').html(to);
            }
        });
    });

    wp.customize('pt_instagram', function (value) {
        value.bind(function (to) {
            if ('to' == '') {
                $('.instagram').hide();
            } else {
                $('.instagram').show();
                $('.instagram').find('a').attr('href', to);
            }
        });
    });

    wp.customize('pt_facebook', function (value) {
        value.bind(function (to) {
            if ('to' == '') {
                $('.facebook').hide();
            } else {
                $('.facebook').show();
                $('.facebook').find('a').attr('href', to);
            }
        });
    });
})(jQuery);
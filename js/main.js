jQuery(document).ready(function ($) {
    /**
     * Mobile menu 
     */
    $(document).on('click','.mobile-menu-btn', function () {
        $('.main-nav-wrapper').toggleClass('active');
    });

    /**
     * Accordion for FAQ-block
     */
    (function() {
        if($('div').is('.faq-block')) {
            var questionsCount = $('.faq-block .question').length;

            if (questionsCount <= 5) {
                $('.faq-block .question').addClass('active');
                $('.faq-block .answer').show();
            }
        }
    })();

    $(document).on('click', '.faq-block .question', function() {
       $(this).toggleClass('active').parent().find('.answer').slideToggle();
    });

    /**
     * Slider
     */
    
    $(document).on('ready', function() {
        $('.jad-slider-block .bxslider').bxSlider({
            pager: false,
            controls: false,
            auto: true,
            speed: 800,
            pause: 7000
        });
    })
});
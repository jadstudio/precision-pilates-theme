<div class="pre-footer">
    <div class="container">
        <div class="widget-area contact-block">
            <h4 class="widget-area-title">Contact Us</h4>
            <?php
            $address = get_option('pt_address');
            $phone = get_option('pt_phone_number');
            $email = get_option('pt_email');
            $copyright = get_option('pt_copyright');
            $facebook = get_option('pt_facebook');
            $instagram = get_option('pt_instagram');
            $twitter = get_option('pt_twitter');
            $linkedin = get_option('pt_linkedin');
            ?>
            <ul class="contacts-list">
                <?php if ($address) { ?>
                    <li class="address">
                        <i class="fa fa-map-marker" aria-hidden="false"></i><span><?php echo $address; ?><span>
                    </li>
                <?php } ?>
                <?php if ($phone) { ?>
                    <li class="phone-number">
                        <i class="fa fa-phone"></i><a href="tel:<?php echo phone_filter($phone); ?>">
                            <?php echo $phone; ?></a>
                    </li>
                <?php } ?>
                <?php if ($email) { ?>
                    <li class="email">
                        <i class="fa fa-envelope"></i><a href="mailto:<?php echo $email; ?>">
                            <?php echo $email; ?></a>
                    </li>
                <?php } ?>
            </ul>

            <ul class="social-links">
                <?php if ($instagram) { ?>
                    <li class = "instagram">
                        <a href="<?php echo $instagram; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </li>
                <?php } ?>
                <?php if ($facebook) { ?>
                    <li class = "facebook">
                        <a href="<?php echo $facebook; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <div class="widget-area info-block">
            <h4 class="widget-area-title">Information</h4>
            <?php wp_nav_menu(array(
                'theme_location' => 'footer_menu',
                'container_class' => 'footer-nav',
                'container' => 'nav',
            )); ?>
        </div>
        <?php dynamic_sidebar('footer_1'); ?>
    </div>
</div>

<footer class="main-footer">
    <div class="container">
        <div class="copy">
            <p><?php echo $copyright; ?></p>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<script src="https://use.fontawesome.com/d5666e2fd8.js"></script>
</body>
</html>
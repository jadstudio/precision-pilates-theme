var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');

gulp.task('build:css', function() {
    gulp.src('styles/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('styles/'));
});

gulp.task('build:css-prod', function() {
    gulp.src('styles/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(gulp.dest('styles/'));
});

gulp.task('build:js', function() {
    return gulp.src(['js/libs/*.js', 'js/main.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('js/'));
});

gulp.task('watch', function() {
    gulp.watch(['styles/**/*.scss'], function() {
        gulp.start('build:css');
    });

    gulp.watch(['js/**/*.js'], function() {
        gulp.start('build:js');
    })
});

gulp.task('build', ['build:css', 'build:js']);

gulp.task('prod', ['build:css-prod', 'build:js']);

gulp.task('start', ['build', 'watch']);
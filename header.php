<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta name="keywords" content="precision pilates, pilates, sport in Mahwah, sport club, private trainings, training, Mel training, education, pilates health" >
    <meta name="description" content="A boutique studio, Precision Pilates is located in a private setting providing one on one instruction.  The studio is fully equipped with Gratz apparatus." >
    <title><?php bloginfo('name') ;?> | <?php the_title(); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
    
    <?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
<header class="main-header">
    <div class="container clearfix">
        <?php echo get_custom_logo();?>


        <div class="contact-block">
            <a href="tel:<?php echo phone_filter(get_option('pt_phone_number')); ?>">
                <span class="register-link">Book Appointment</span>
                <span class="phone-number"><i class="fa fa-phone"></i><?php echo get_option('pt_phone_number'); ?></span>
            </a>
        </div>

        <div class= "main-nav-wrapper">
            <?php wp_nav_menu(array(
                'menu'            => 'header_menu',
                'container'       => 'nav',
                'container_class' => 'main-nav'
            )); ?>
        </div>

        <div class="mobile-menu-btn">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>
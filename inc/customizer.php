<?php
add_action('customize_register', 'pt_custom_contact_option');
add_action('customize_register', 'pt_custom_social_option');
function pt_custom_contact_option($wp_customize)
{

    $wp_customize->add_section('pt_contact_data_section', array(
        'title' => __('Contact Info', 'pilates'),
        'priority' => 20,
        'description' => __('Contact Data', 'pilates'),
    ));

    $wp_customize->add_setting(
        'pt_phone_number',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'pt_phone_number',
        array(
            'label' => __('Phone Number', ''),
            'section' => 'pt_contact_data_section',
            'settings' => 'pt_phone_number',
            'priority' => 2,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'pt_address',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
//            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'pt_address',
        array(
            'label' => __('Address', ''),
            'section' => 'pt_contact_data_section',
            'settings' => 'pt_address',
            'priority' => 1,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'pt_email',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'pt_email',
        array(
            'label' => __('Email', ''),
            'section' => 'pt_contact_data_section',
            'settings' => 'pt_email',
            'priority' => 3,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'pt_copyright',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'pt_copyright',
        array(
            'label' => __('Copyright', ''),
            'section' => 'pt_contact_data_section',
            'settings' => 'pt_copyright',
            'priority' => 4,
            'type'        => 'text',
        )
    ));

}

function pt_custom_social_option($wp_customize)
{

    $wp_customize->add_section('pt_social_section', array(
        'title' => __('Social links', 'pilates'),
        'priority' => 20,
        'description' => __('Social links', 'pilates'),
    ));

    $wp_customize->add_setting(
        'pt_facebook',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'pt_facebook',
        array(
            'label' => __('Facebook', ''),
            'section' => 'pt_social_section',
            'settings' => 'pt_facebook',
            'priority' => 2,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'pt_instagram',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
//            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'pt_instagram',
        array(
            'label' => __('Instagram', ''),
            'section' => 'pt_social_section',
            'settings' => 'pt_instagram',
            'priority' => 1,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'pt_twitter',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'pt_twitter',
        array(
            'label' => __('Twitter', ''),
            'section' => 'pt_social_section',
            'settings' => 'pt_twitter',
            'priority' => 3,
            'type'        => 'text',
        )
    ));

    $wp_customize->add_setting(
        'pt_linkedin',
        array(
            'default' => '',
            'type'    => 'option',
            'transport' => 'postMessage',
            'sanitize_callback' => '',
        )
    );


    $wp_customize->add_control(new WP_Customize_Control(
        $wp_customize,
        'pt_linkedin',
        array(
            'label' => __('Linkedin', ''),
            'section' => 'pt_social_section',
            'settings' => 'pt_linkedin',
            'priority' => 4,
            'type'        => 'text',
        )
    ));

}

add_action( 'customize_preview_init', 'pt_customizer_script' );
function pt_customizer_script() {
    wp_enqueue_script( 'pt-customizer-script', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ) );
}